checkHeaderBehavior();

function checkHeaderBehavior() {
    if (localStorage.getItem("headerBehavior") === null)
        localStorage.setItem("headerBehavior", "scrolling-header");

    if (localStorage.getItem("headerBehavior") === "scrolling-header") {
        document.body.classList.remove('fixed-header');
        document.body.classList.add('scrolling-header');
    } else {
        document.body.classList.remove('scrolling-header');
        document.body.classList.add('fixed-header');
    }
}

function toggleHeaderBehavior() {
    if (localStorage.getItem("headerBehavior") === null)
        localStorage.setItem("headerBehavior", "scrolling-header");

    if (localStorage.getItem("headerBehavior") === "scrolling-header") {
        localStorage.setItem("headerBehavior", "fixed-header");
    } else {
        localStorage.setItem("headerBehavior", "scrolling-header");
    }
    checkHeaderBehavior();
}

checkHeaderType();

function checkHeaderType() {
    if (localStorage.getItem("headerType") === null)
        localStorage.setItem("headerType", "default-header");

    if (localStorage.getItem("headerType") === "default-header") {
        document.body.classList.remove('min-header');
        document.body.classList.add('default-header');
    } else {
        document.body.classList.remove('default-header');
        document.body.classList.add('min-header');
    }
}

function toggleHeaderType() {
    if (localStorage.getItem("headerType") === null)
        localStorage.setItem("headerType", "default-header");

    if (localStorage.getItem("headerType") === "default-header") {
        localStorage.setItem("headerType", "min-header");
    } else {
        localStorage.setItem("headerType", "default-header");
    }
    checkHeaderType();
}

checkFooterBehavior();

function checkFooterBehavior() {
    if (localStorage.getItem("footerBehavior") === null)
        localStorage.setItem("footerBehavior", "scrolling-footer");

    if (localStorage.getItem("footerBehavior") === "scrolling-footer") {
        document.body.classList.remove('fixed-footer');
        document.body.classList.add('scrolling-footer');
    } else {
        document.body.classList.remove('scrolling-footer');
        document.body.classList.add('fixed-footer');
    }
}

function toggleFooterBehavior() {
    if (localStorage.getItem("footerBehavior") === null)
        localStorage.setItem("footerBehavior", "scrolling-footer");

    if (localStorage.getItem("footerBehavior") === "scrolling-footer") {
        localStorage.setItem("footerBehavior", "fixed-footer");
    } else {
        localStorage.setItem("footerBehavior", "scrolling-footer");
    }
    checkFooterBehavior();
}