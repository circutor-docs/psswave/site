function showToast(size, type, message = 'Dummy message', details = null) {
    var toastContainer = document.getElementById('toast-id');
    if (toastContainer == null)
        toastContainer = document.createElement('div');

    toastContainer.className = 'cds-toast-container';
    toastContainer.id = 'toast-id';

    const toast = document.createElement('div');
    toast.className = 'cds-toast cds-toast--' + size + ' cds-toast--' + type;

    var toastIcon = 'cds-ico-check-circle-solid';
    if (type == 'error')
        toastIcon = 'cds-ico-close-circle-solid';

    if (type != 'minimal') {
        const toasticon = document.createElement('span');
        toasticon.className = 'cds-toast__icon ' + toastIcon;
        toast.appendChild(toasticon);
    }

    const toastcontent = document.createElement('div');
    toastcontent.className = 'cds-toast__content';

    if (message != null) {
        const toastcontentmessage = document.createElement('span');
        toastcontentmessage.className = 'cds-toast__content__message';
        toastcontentmessage.textContent = message;
        toastcontent.appendChild(toastcontentmessage);
    }

    if (details != null) {
        const toastcontentdetails = document.createElement('span');
        toastcontentdetails.className = 'cds-toast__content__details';
        toastcontentdetails.textContent = details;
        toastcontent.appendChild(toastcontentdetails);
    }

    toast.appendChild(toastcontent);
    toastContainer.appendChild(toast);
    document.body.appendChild(toastContainer);

    setTimeout(() => {
        toast.classList.add('show');
    }, 1);

    setTimeout(() => {
        toast.classList.remove('show');
        setTimeout(() => {
            toastContainer.removeChild(toast);
            toastContainer.hasChildNodes() ? null : document.body.removeChild(toastContainer);
        }, 1000);
    }, 300000);
}
