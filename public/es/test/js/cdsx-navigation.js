let navItem = document.querySelectorAll(".navLink");
document.addEventListener("click", handleClickOutside);

function handleClickOutside(e) {
    if (document.body.classList.contains("collapsed-sidebar")) {
        let closestnavItem = e.target.closest(".navItem");
        document.querySelectorAll('.navItem.navItemOpen').forEach(item => {
            if (item !== closestnavItem) {
                item.classList.remove('navItemOpen');
            }
        });
    }
}


for (var i = 0; i < navItem.length; i++) {
    navItem[i].addEventListener("click", (e) => {
        let closestnavItem = e.target.closest(".navItem");
        document.querySelectorAll('.navItem.navItemOpen').forEach(item => {
            if (item !== closestnavItem) {
                item.classList.remove('navItemOpen');
            }
        });
        handleClickOutside(e); //Check only when collapsed-sidebar
        closestnavItem.classList.toggle("navItemOpen");
    });
}
