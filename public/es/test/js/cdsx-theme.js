if (localStorage.getItem("cds-theme") == null) {
    localStorage.setItem("cds-theme", "system");
}
switch (localStorage.getItem("cds-theme")) {
    case "light":
        document.getElementById("lightTheme").checked = true;
        break;
    case "dark":
        document.getElementById("darkTheme").checked = true;
        break;
    case "system":
        document.getElementById("systemTheme").checked = true;
        break;
}
applyTheme();

document.getElementsByName("themeRadio").forEach(function (radio) {
    radio.addEventListener("change", function () {
        if (this.checked) {
            setLocalStorage(this.value);
        }
    });
});

function setLocalStorage(theme) {
    localStorage.setItem("cds-theme", theme);
    applyTheme();
}

window.matchMedia("(prefers-color-scheme: dark)").addEventListener("change", (e) => {
    applyTheme();
});

function applyTheme() {
    const localStorageTheme = localStorage.getItem("cds-theme");
    if (localStorageTheme != "system") {
        window.document.body.setAttribute("data-theme", localStorageTheme);
    } else {
        const prefersDarkMode = window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches;
        const systemMode = prefersDarkMode ? "dark" : "light";
        window.document.body.setAttribute("data-theme", systemMode);
    }
}