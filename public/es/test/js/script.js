
document.addEventListener("DOMContentLoaded", function () {


  // Get the buttons and dropdowns
  var buttons = document.querySelectorAll(".cds-dropdown__trigger");
  var dropdowns = document.querySelectorAll(".cds-dropdown__content");

  // Toggle dropdown visibility when clicking the button
  buttons.forEach(function (button, index) {
    button.addEventListener("click", function () {
      // Show the clicked dropdown
      dropdowns[index].classList.toggle("cds-dropdown__content--visible");

      // Hide all other dropdowns
      dropdowns.forEach(function (otherDropdown, otherIndex) {
        if (otherIndex !== index) {
          otherDropdown.classList.remove("cds-dropdown__content--visible");
        }
      });
    });
  });

  // Hide dropdowns when clicking outside any dropdown
  document.addEventListener("click", function (event) {
    dropdowns.forEach(function (dropdown) {
      if (
        !dropdown.contains(event.target) &&
        !Array.from(buttons).some((button) => button.contains(event.target))
      ) {
        dropdown.classList.remove("cds-dropdown__content--visible");
      }
    });

    /////////////////////TREEVIEW
    var treeItems = document.querySelectorAll(".cds-tree__item");

    treeItems.forEach(function (item) {
      item.addEventListener("click", function (event) {
        treeItems.forEach(function (parentItem) {
          // Close others but its parent
          if (parentItem !== item && !parentItem.contains(item)) {
            parentItem.classList.remove("is-opened");
          }
          //Unselect all others
          parentItem.classList.remove("is-selected");
        });

        event.stopPropagation();
        item.classList.toggle("is-opened");
        item.classList.toggle("is-selected");

        //Do not mark as opened if lastchild is null
        if (item.lastElementChild.className === "cds-tree__node") {
          item.classList.remove("is-opened");
        }
      });
    });

    /////////////////////NAVIGATION
    var navItems = document.querySelectorAll(".cdsx-navigation__item");

    navItems.forEach(function (item) {
      item.addEventListener("click", function (event) {
        // Close all other opened options
        navItems.forEach(function (otherItem) {
          if (otherItem !== item && !otherItem.contains(item)) {
            otherItem.classList.remove("is-opened");
          }
        });

        event.stopPropagation();
        item.classList.toggle("is-opened");
      });
    });
  });
});
